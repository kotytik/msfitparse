var openFile = function(event) {
    var input = event.target;

    var reader = new FileReader();
    reader.onload = function(){

        var properties = reader.result
            .split('\r\n')[0].split(',');
        // console.log(properties);
        window.personals = reader.result
            .split('\r\n');
        personals.splice(0,1);
        // console.log(personals);
        var personalPropery = ['first_name',
            'last_name',
            'phone',
            'email',
            'password',
            'birth_date',
            'sex',
            'height'
        ];
        personals =personals.map((item)=>{
            return item
                .split(',')
                .reduce(
                    (result,item,id)=>{

                        result[properties[id].toUpperCase()]=item;
                        return result
                    },{})});

        // console.log(personals);
        document.getElementById('resCsvTable').innerHTML = reader.result
            .split('\n')
            .reduce(
                (result,csvString) => {

                    return result+'<tr>'+csvString.split(',')
                        .reduce(function (Html, csvitem,id) {

                            return ((properties[id]=='PROFILE' || properties[id]=='test_date') && csvitem=='')?Html+'<td bgcolor="red">'+csvitem+'</td>':Html+'<td>'+csvitem+'</td>';
                        },'')+'</tr>';

                },''
            )

    };
    reader.readAsText(input.files[0]);
};
var sendCsv = function () {
    $.ajax({
        type: "POST",
        url: "importModel.php",
        data: {models:JSON.stringify(window.personals)},
        async: false,
        success: function (data) {
            console.log(data)
        }
    })
};
